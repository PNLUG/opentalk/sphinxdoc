# La documentazione tecnica con Sphinx

## Italiano

Presentiamo un breve tutorial per imparare a scrivere documentazione tecnica
utilizzando [Sphinx](https://www.sphinx-doc.org/en/master/)

Sphinx è scritto in [Python](https://www.python.org/) ed è quindi molto usato
per documentare librerie scritte in quel linguaggio di programmazione, ma è
ottimo anche per scrivere documentazioni generiche.

Il tutorial si compone di:

- Il progetto Sphinx del tutorial
- Un progetto di prova
- Il sito html compilato

Per leggere velocemente il tutorial scaricare il progetto, andare nella
cartella: **./build_doc/html** ed aprire nel browser il file **index.html**

Da li si impara come compilare con Sphinx.

## English

We present a short tutorial to learn how to write technical documentation
using [Sphinx](https://www.sphinx-doc.org/en/master/)

Sphinx is written in [Python](https://www.python.org/) and it is widely used
to document libraries written in that programming language, but is excellent
also to write generic documentations.

The tutorial is composed by:

- The Sphinx project of the tutorial
- A test project
- The html compiled site

To quickly read the tutorial, download the project, go to folder:
**./build_doc/html** then open the file **index.html** in the browser.

From there you will learn how to compile with Sphinx.

Translations are welcome.
