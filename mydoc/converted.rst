Prova di un testo
=================

Usiamo alcuni **metalinguaggi** per scrivere html in forma leggibile e
semplificata

Ecco alcuni esempi:

-  `markdown <https://www.markdownguide.org/>`__
-  `restructured text <https://docutils.sourceforge.io/rst.html>`__
-  `pandoc <https://pandoc.org/>`__

per convertire:

::

    $ pandoc test.md -o converted.rst
    $ pandoc converted.rst -o converted.md
    $ pandoc -s converted.md -o converted.html
