.. Piccolo esempio con alcune delle funzionalità di Sphinx. In questa pagina
   abbiamo inserito una immagine indentata rispetto al margine. Ed abbiamo
   aggiunto alcune pagine

La prima documentazione con Sphinx's
====================================

   .. image:: _static/giza.jpeg
      :scale: 80
      :alt: La sfinge a Giza

Facciamo qualche prova con Spinx!

.. toctree::
   :maxdepth: 2
   :caption: Contenuto:

   converted.rst
   test.md
   sezione1/index.rst
   pythondoc.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. il `modindex` viene popolato dal sistema di documentazione dei moduli python
   se il progetto non contiene codice python si può cancellare.