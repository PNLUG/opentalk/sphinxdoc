""" MyLib it's only a dummy python module.
"""

class FooBar:
    """FooBar class do nothing
    """

    def __init__(self):
        """Completely useless
        """
        pass

    def foo(self):
        """my super foo method
        """
        pass

    def bar(self):
        """my super bar method
        """
        pass

def foobar():
    """Really nothing to do
    """
    pass