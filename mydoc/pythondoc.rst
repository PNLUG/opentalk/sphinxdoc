Documentazione Python
=====================

Questo è l'esempio più semplice possibile per documentare un modulo python

.. automodule:: mylib
    :members:
