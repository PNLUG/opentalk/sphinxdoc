Pagina 1
========

Testo della pagina 1 della |section|

Linux è il nostro :abbr:`OS (Operating System)` preferito.

Oggi è il |today|

E questa documentazione è la Release |release|

Il mio sito è |my_url|


.. si possono definire macro di sostituzione che valgono per tutto il testo.
   nel file di configurazione conf.py si possono dichiarare macro che valgono
   per tutte le pagine
   le macro possono essere inserire in qualsiasi posizione e valgono per
   tutto il testo, quindi può essere conveniente raggrupparle tutto a fondo
   pagina.

.. |section| replace:: sezione 1

