# Prova di un testo

Usiamo alcuni **metalinguaggi** per scrivere html in forma leggibile e semplificata

Ecco alcuni esempi:

- [markdown](https://www.markdownguide.org/)
- [restructured text](https://docutils.sourceforge.io/rst.html)
- [pandoc](https://pandoc.org/)



per convertire:

    $ pandoc test.md -o converted.rst
    $ pandoc converted.rst -o converted.md
    $ pandoc -s converted.md -o converted.html


