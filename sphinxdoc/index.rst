.. Introduzione a Sphinx documentation master file, created by
   sphinx-quickstart on Wed Apr 22 09:02:49 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

La documentazione tecnica con Sphinx
====================================

In questa breve introduzione presenteremo `Sphinx
<https://www.sphinx-doc.org/en/master/index.html>`__ che è un sistema per la
scrittura di documentazione tecnica scritto in linguaggio
`Python <https://www.python.org/>`__

.. image:: /_static/sphinxheader.png

.. toctree::
   :maxdepth: 3
   :caption: Contenuto:

   pagine/premessa.rst
   pagine/installazione.rst
   pagine/scrivere.rst
   pagine/configurazione.rst
   pagine/glossario.rst
   pagine/risorse.rst

* :ref:`genindex`
* :ref:`search`

