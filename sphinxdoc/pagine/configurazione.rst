MarkDown
========

È giunto il momento di parlare nuovamente di :ref:`MarkDown <md_ref>`.

Come dicevamo Sphinx ha una forte integrazione con rst e riesce a gestire molti
automatismi, però abbiamo anche detto che MarkDown è molto diffuso, perciò può
capitare di avere la necessità di condividere files MarkDown tra Sphinx ed
altri sistemi, o semplicemente disponiamo già di molti testi che non abbiamo
voglia di tradurre.

Per venire incontro a queste esigenze, Sphinx fornisce la possibilità di
integrare testi MarkDown, che però non potranno usufruire di tutte le
prerogative di cui godono i files rst.

Per attivare MarkDown dobbiamo importare il modulo apposito con:

::

    $ pip install recommonmark

In realtà lo avevamo gia fatto quando abbiamo parlato di :doc:`Installazione
</pagine/installazione>`.

Poi si debbono fare alcune modifiche al file di configurazione ``conf.py`` di
cui parleremo tra poco in modo più approfondito. Queste sono le modifiche:

::

    extensions = ['recommonmark']

    source_suffix = {
        '.rst': 'restructuredtext',
        '.txt': 'markdown',
        '.md': 'markdown',
    }

A questo punto solo aggiungendo il file nel ``.. toctree::``, il documento
verrà aggiunto con la sintassi appropriata.


Configurazione
==============

Il file conf.py
---------------

La configurazione di Sphinx viene fatta con un file Python di nome ``conf.py``
che si trova nella stessa directory del progetto. il comando:

::

    $ sphinx-quickstart

Ne ha già preparata una versione funzionante con i parametri principali a cui
possiamo aggiungere i parametri di nostra scelta.

La lista completa dei parametri attivabili si trova in questa
`pagina. <https://www.sphinx-doc.org/en/master/usage/configuration.html>`_ qui
ne elenchiamo solo alcuni.

**extenions** permette di attivare estensioni integrate o di terze parti, ad
esempio:

::

    extensions = [
        'recommonmark',
        'sphinx.ext.autosectionlabel'
    ]

    autosectionlabel_maxdepth = 3

attiva due estensioni. La seconda permette di interrogare il parametro
``autosectionlabel_maxdepth`` che indica il livello di profondità dell'albero
dell indice.

**rst_prolog** e **rst_epilog** permettono di aggiungere un testo fisso a tutte
le pagine rst in testa e/o in coda a ciascuna pagina. Ad esempio

::

    rst_epilog = """
    Questo documento è sottoposto a licenza creative commons

    .. image:: /_static/creative_commons.png

    .. |my_url| replace:: https://mywebagency.com

    .. |br| raw:: html

    <br />

    """

Questa è una posizione buona per inserire direttive che possono essere utili
in tutte le pagine.

**smartquotes** esegue la sostituzione automatica dei caratteri di quotatura
``"`` e ``'`` con caratteri tipografici, ma a volte possono essere fastidiosi,
ad esempio se scrivo ``un po'`` mi ritrovo scritto ``un po"``. per evitare
posso aggiungere

::

    smartquotes = False

il tema html
------------

Sempre nel file ``conf.py`` c'è la possibilità di scegliere il tema html.

Alcuni temi sono già precaricati con sphinx ed è sufficiente cambiare il
tema 'alabaster' che è quello predefinito con quello scelto, ad esempio

::

    # html_theme = 'alabaster'
    html_theme = 'sphinxdoc'

I temi opzionali invece devono essere scaricati con pip e dichiarati nelle
estensioni, ad esempio:

::

    $ pip install sphinx-rtd-theme

e poi in ``conf.py`` si fanno queste modifiche:

::

    import sphinx_rtd_theme

    extensions = [
        ...
        "sphinx_rtd_theme",
    ]

    html_theme = "sphinx_rtd_theme"


Documentazione di Python
========================

Sphinx dopotutto nasce per documentare i moduli Python, quindi è normale che
ci siamo facilitazioni apposite per questo scopo.

C'è comunque da dire che Python ci aiuta molto, infatti ha integrato nel
linguaggio stesso un sistema di introspezione della documentazione molto
efficace su cui Sphinx si appoggia per generare la documentazione automatica.

Troviamo tutto nella pagina di `autodoc <https://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html#module-sphinx.ext.autodoc>`_

Per prima cosa doppiamo aggiungere l'apposita estensione.

::

    extensions = [
        ...
        'sphinx.ext.autodoc',
    ]

poi bisogna che il modulo da documentare sia disponibile nel percorso di
ricerca di Python.

Se il file da documentare risiede nella stessa cartella del progetto Sphinx,
posso modificare il file ``conf.py`` aggiungendo queste righe:

::

    import os
    import sys
    sys.path.insert(0, os.path.abspath('.'))

A questo punto nel file in cui vogliamo che compaia la documentazione possiamo
semplicemente aggiungere la direttiva:


::

    .. automodule:: mylib
        :members:

in cui ``mylib`` è il nome della libreria da documentare.

Estensioni di Sphinx
====================

Puo capitare che le funzionalità di cui abbiamo bisogno per la nostra
documentazione non sia immediatamente presente nella configurazione standard.

Sphinx dispone di molte estensioni, e nel corso di questa presentazione
ci è già capitato di utilizzarne.

Alcune sono già disponibili con la installazione standard, altre invece sono
di terze parti e prima di poterle utilizzare devono essere installate.

Nella `pagina <http://www.sphinx-doc.org/en/master/usage/extensions/index.html>`_
dedicata alle estensioni si possono trovare maggiori informazioni, compresa
la lista delle estensioni disponibili ed anche la documentazione per sviluppare
le proprie se non si trova quella adatta al nostro scopo.
