
Glossario
=========

.. glossary::
   :sorted:

   Sphinx
      Sistema per la scrittura di documentazione tecnica scritto in Python

   Python
      Linguaggio di programmazione ad alto livello usato principalmente per
      scripting, web, scripting embedded ed altro.

   IDE
      (Integrated Development Environment) Gli IDE sono programmi che offrono
      funzionalità integrate per lo sviluppo del software in modo da disporre
      in un unico ambiente tutte le funzionalità necessarie allo sviluppo.

   HTML
      Linguaggio di markup utilizzato per i documenti web

   MarkDown
      Linguaggio di markup che facilita la stesura di testi html

   RestructuredText
      Linguaggio di markup che facilita la stesura di testi html

   Siti statici
      Siti composti da soli files distribuiti senza modifiche

   Siti dinamici
      Siti in cui parte del contenuti vengono generati automaticamente nel
      momento in cui viene fatta la richiesta

   Static Site Generator
      Sistema o programma in grado di generare siti statici a partire da
      files di markup e di configurazione.

   .rst
      Solitamente le pagine scritte in RestructuredText vengono salvate con
      l'estensione .rst questo aiuta a riconoscere il formato dei files ed
      aiuta i programmi che ne fanno uso.

   .md
      Solitamente le pagine scritte in MarkDown  vengono salvate con
      l'estensione .md questo aiuta a riconoscere il formato dei files ed
      aiuta i programmi che ne fanno uso.
