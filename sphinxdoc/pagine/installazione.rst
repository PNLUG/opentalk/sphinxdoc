Installazione di Spinx
======================

In questa breve presentazione illustriamo come installare Sphinx e preparare
un nuovo progetto di documentazione.

Sphinx è una libreria python ed i comandi necessari devono essere impartiti
da terminale. Questo non è un corso su python, quindi non mi dilungo sul
significato dei comandi python sui quali invito tutti ad approfondire in
proprio.

Python è multi piattaforma e Sphinx funziona su tutte le piattaforme, ma i
comandi possono essere leggermente diversi, in questo tutorial useremo come
ambiente di riferimento Python 3.6 su Linux Ubuntu 18.04, ma i comandi
dovrebbero essere uguali su tutti i sistemi basati su Debian.

Per le altre piattaforme non è difficile trovare i comandi equivalenti facendo
una piccola ricerca.

Installazione
-------------

Incominciamo. Prima di tutto vediamo se abbiamo Python e tutto quello che
serve.

::

    $ sudo apt install python3
    $ sudo apt install python3-dev
    $ sudo apt install python3-pip
    $ sudo apt install python3-venv

Molto probabilmente python3 è gia installato, ma pip e venv potrebbero non
esserlo.

Ora creiamo una directory e prepariamo il nostro progetto:

::

    $ cd ~
    $ mkdir sphinxdoc
    $ cd sphinxdoc
    $ python3 -m venv venv
    $ source venv/bin/activate
    $ pip install -U pip
    $ pip install sphinx
    $ pip install recommonmark
    $ pip install sphinxcontrib.websupport

Con questi comandi abbiamo creato un ambiente virtuale isolato che evita
conflitti con altre eventuali istanze di python. Ed abbiamo anche installato
tutto ciò che ci serve per partire. Se serviranno altre librerie le
installeremo in seguito.

L'ambiente è creato, se ora usciamo del terminale, la prossima volta che
rientriamo ci sarà sufficiente dare i comandi:

::

    $ cd ~
    $ cd sphinxdoc
    $ python3 -m venv venv
    $ source venv/bin/activate


Il primo progetto
-----------------

A questo punto possiamo creare il nostro progetto:

::

    $ mkdir mydoc
    $ cd mydoc
    $ sphinx-quickstart

Ora la macchina ci chiede alcune informazioni. Possiamo inserire i dati
senza problemi anche perché se dovessimo cambiare idea possiamo sempre andare
a modificare il file di configurazione che è stato creato.

Alla fine il sistema avrà scritto un po' di files che sono sufficienti per
compilare il nostro primo progetto funzionante. Se digitiamo:

::

    $ make

Sphinx ci elenca la lista dei formati di documentazione che è in grado di
creare, per alcuni di questi però sono necessarie ulteriori configurazioni.

Per creare un sito html digitiamo:

::

    $ make html

Se non ci sono errori vengono compilati i sorgenti per produrre il
risultato finale. Possiamo aprire il sito con:

::

    $ cd _build/html/
    $ firefox index.html

e dovremmo vedere la nostra home page della documentazione. Naturalmente
invece che aprire firefox da terminale, possiamo aprire le cartelle da
interfaccia grafica e fare doppio click su `index.html`.

Markdown e Pandoc
-----------------

Ora possiamo iniziare ad aggiungere qualche contenuto. Ma prima torniamo a
parlare di markdown un momento.

In :ref:`precedenza <md_ref>` avevamo detto che sphinx makrdown con alcune
limitazioni. Avevamo parlato anche di :ref:`pandoc_ref` che è in grado di
tradurre in vari formati, quindi direi che possiamo fare una prova.

Prima di tutto se non lo abbiamo già, installiamo Pandoc con:

::

    $ sudo apt install pandoc

Scriviamo quindi una piccola pagina di nome `test.md` con questo contenuto:

::

    # Prova di un testo

    Usiamo alcuni **metalinguaggi** per scrivere html in forma leggibile e semplificata

    Ecco alcuni esempi:

    - [markdown](https://www.markdownguide.org/)
    - [restructured text](https://docutils.sourceforge.io/rst.html)
    - [pandoc](https://pandoc.org/)

    per convertire:

        $ pandoc test.md -o converted.rst
        $ pandoc converted.rst -o converted.md
        $ pandoc -s converted.md -o converted.html


poi la convertiamo in rst e nuovamente in markdown ed html con i seguenti
comandi:

::

    $ pandoc test.md -o converted.rst
    $ pandoc converted.rst -o converted.md
    $ pandoc -s converted.md -o converted.html

I documenti sono stati convertiti, una cosa che possiamo notare è che
`converted.md` è un po' diverso da `test.md`, e questo è normale.


La prima pagina
---------------

È giunto il momento di aggiungere contenuti alla nostra documentazione. Per
prima cosa aggiungeremo la pagina rst appena creata e per fare questo è
sufficiente modificare la pagina `index.rst`. Dato che ci siamo modifichiamo
un po' il testo ed inseriamo una immagine. Il nuovo testo sarà questo:

::

    La prima documentazione con Sphinx's
    ====================================

    .. image:: /_static/giza.jpeg

    Facciamo qualche prova con Spinx!

    .. toctree::
        :maxdepth: 2
        :caption: Contenuto:

        converted.rst

    Indices and tables
    ==================

    * :ref:`genindex`
    * :ref:`modindex`
    * :ref:`search`


Il posto usuale in cui inserire le immagini e gli altri contenuti è la
cartella _static ed è quello che abbiamo fatto. Ora ricompiliamo con:

::

    $ make html

e facciamo il refresh della pagina.

Ora proviamo ad introdurre un errore, modifichiamo la riga

::

        converted.rst

con

::

        converted_page.rst

Se proviamo a ricompilare, il sistema ci avverte che c'è un errore e non fà
l'inclusione.


La documentazione di Sphinx
---------------------------

Qual'è la documentazione Sphinx scritta in modo migliore?

**ESATTO!** Avete indovinato.

È la documentazione di Sphinx stesso. Perciò potrebbe non essere una cattiva
idea quella di scaricare sphinx e ricompilare la documentazione allo scopo
di studiare come è stata strutturata. Per fare questo si scaricano i sorgenti
dal sito https://github.com/sphinx-doc/sphinx poi si scompatta dentro al nostro
ambiente venv e come al solito si ricompila:

::

    $ cd sphinx-3.x/doc/
    $ make html

Ora abbiamo una versione locale della documentazione che ci sarà molto utile
per vedere come fare le cose al meglio.
