Premessa
========

La documentazione tecnica
-------------------------

La documentazione tecnica è molto importante. Qualunque sia il progetto che
state sviluppando, la documentazione è una parte fondamentale che può
contribuire grandemente al successo.

Una buona documentazione tecnica dovrebbe rendere il più facile possibile
reperire le informazioni da parte dei lettori i quali possono leggere in modo
sequenziale, oppure anche cercando gli argomenti specifici di proprio
interesse.

Per questo motivo è necessario disporre di un buon sistema di ricerca e può
anche essere utile che siano presenti glossario ed indice analitico.alitico.

Il sistema deve anche essere agevole per l'autore che chiede di scrivere
rapidamente, di fare pochi errori di sintassi e di ristrutturare facilmente
i testi.

Strumenti
---------

Esistono molti strumenti per scrivere la documentazione ne elenchiamo alcuni
organizzati per tipologia.

Testo semplice
..............

Di solito il primo metodo per approcciare la documentazione è quello di
scrivere degli appunti su come desideriamo strutturare i nostri documenti.

Per fare questo io preferisco usare semplici files di testo da usare come
traccia per documenti più strutturati. I programmi per scrivere testo semplice
sono innumerevoli. Si fa dal più semplice "Notepad" presente in Windows fino
ai più sofisticati editor per programmatori. Io ultimamente preferisco
`Visual Studio Code <https://code.visualstudio.com/>`__ che oltre che per
scrivere testi semplici può essere usato come IDE per molti linguaggi di
programmazione.

Naturalmente questi programmi non hanno strutture per scrivere documenti
organizzati ma possono essere utili al più per testi brevi.

LibreOffice e gli altri
.......................

Molti usano scrivere la documentazione con programmi di trattamento testi
come `LibreOffice Writer <https://it.libreoffice.org/>`_ oppure con sistemi
tipografici come `Scribus <https://www.scribus.net/>`_.

La caratteristica principale di questi programmi è che sono orientati alla
stampa e solitamente utilizzano files in formato binario che quindi possono
essere aperti solo con il programma che li ha generati oppure con programmi
compatibili.

Questi programmi sono anche in grado di effettuare esportazioni in formato
html, ma il risultato è solitamente scadente e dal mio punto di vista, per
la documentazione tecnica la capacità di produrre testi html di qualità è
un fattore decisivo.

Html ed il web
..............

Una buona documentazione tecnica, secondo molti, dovrebbe essere scritta in
formato html, in modo da essere pubblicata nel web dove i potenziali lettori
possono trovare facilmente i documenti di proprio interesse.

Il linguaggio html non è complesso e nella sua versione originale era stato
pensato proprio per la produzione di documentazione tecnica.

Però per scrivere direttamente in formato html, si devono rispettare alcune
regole e la leggibilità diretta dei testi sorgente non è elevatissima.

Si deve anche considerare che nel web moderno ci sono altri componenti che
concorrono alla scrittura dei documenti che rendono più complessa la redazione
dei testi.

Infine nella scrittura diretta di codice html non sono disponibili gli
automatismi per la documentazione così utili a lettori e redattori.

.. _md_ref:

Linguaggi intermedi (Markdown e rst)
....................................

Per scrivere testi html in modo più facile e leggibile sono stati sviluppati
linguaggi di markup più semplici da scrivere e leggere che possono essere
facilmente compilati per produrre testi html pronti per il web.

Al momento il più più diffuso ed usato è sicuramente `Markdown
<https://www.markdownguide.org/>`__ che è particolarmente facile da usare.

Il sistema Sphinx che stiamo descrivendo è in grado di usare MarkDown ma solo
in versione basica, quindi rinunciando a molte delle funzionalità avanzate di
cui parleremo più avanti.

Il linguaggio di markup usato da Sphinx è `Restructured Text
<https://docutils.sourceforge.io/rst.html/>`__ che probabilmente è un po' meno
intuitivo, ma ha una sintassi più ricca e può essere configurato in modo
versatile utilizzando apposite direttive.

.. _pandoc_ref:

Pandoc
......

Parlando di linguaggi di markup e di metalinguaggi, non si può non citare
`Pandoc <https://pandoc.org/>`__ che è un sistema di traduzione automatica di
testi.

Con Pandoc ad esempio si può trasformare un testo MarkDown in un testo
RestructuredText o html e viceversa.

È quindi uno strumento utilissimo sia per traduzioni estemporanee che per
sistemi organizzati di produzione di testi.

Naturalmente le traduzioni vengono fatte "al minimo", da una traduzione ad un
altra inevitabilmente si perdono alcune caratteristiche.

Per fare un esempio, se ho un libro scritto in giapponese, la traduzione in
inglese per forza di cose porterà ad una perdita semantica. Poi se lo si vuole
tradurre anche in italiano, se si parte dalla traduzione inglese si avrà una
perdita ulteriore, se invece si parte dal originale la perdita sarà minore,
ma sarà comunque inevitabile.


Siti statici
............

Negli ultimi periodi sono comparsi sistemi per realizzare siti statici come ad
esempio `Hugo <https://gohugo.io/>`__ o `altri <https://www.staticgen.com/>`__
in grado di produrre interi siti web a partire da testi MarkDown o
RestructuredText ed a files di configurazione.

Lo scopo principale dei generatori di siti, normalmente, non è quello di
produrre siti di documentazione, anche se a volte vengono usati anche per
questo, ma quello di produrre siti esteticamente accettabili con sezioni
dedicate ai blog e che al contempo abbiano pochi requisiti dal lato server
per favorire prestazioni e manutenibilità.


Sphinx
------

`Sphinx <https://www.sphinx-doc.org/en/master/index.html>`__ è tecnicamente
un generatore di siti statici, ma a differenza dei precedenti è orientato alla
produzione di documentazione tecnica.

È scritto linguaggio `Python <https://www.python.org/>`__ perciò è naturalmente
orientato alla scrittura di documentazione su librerie Python ma viene
apprezzato anche per altri tipi di documentazione.

È un prodotto maturo e di successo e questo è testimoniato dai molti progetti
che lo usano. La documentazione di Sphinx ne cita
`alcuni <https://www.sphinx-doc.org/en/master/examples.html>`__

Un altro sito degno di nota è `Read the Docs <https://readthedocs.org/>`__ che
utilizza Sphinx per ospitare moltissimi siti di documentazione. Anche in questo
caso la gran parte dei documenti si riferiscono a progetti che fanno parte
dell'ecosistema Python ma non mancano documenti tecnici generici. Ecco alcuni
esempi.

https://readthedocs.org/projects/bootstrap-datepicker/

https://readthedocs.org/projects/scrapy/

Dopo aver scritto la documentazione, è possibile scegliere l'aspetto visivo tra
vari templates disponibili. Alcuni sono integrati nel sistema, altri si
possono scaricare. Ad esempio il sito `Read the Docs
<https://readthedocs.org/>`__ di cui abbiamo parlato prima ha sviluppato un
proprio template che riscuote un discreto successo.

Anche in questo caso i templates non possono competere esteticamente con i temi
di famosi sistemi orientati ai siti web, ma sicuramente offrono il massimo
dell'efficacia per lo scopo che si prefiggono mettendo a disposizione dei
lettori una esperienza visiva gradevole ed una navigazione efficiente delle
informazioni.

Un'altra caratteristica di Sphinx è che è in grado di produrre documenti anche
in formati diversi come *pdf*, *epub* ed altri ed anche a partire dagli stessi
sorgenti, quindi si possono ottenere contemporaneamente più formati.


