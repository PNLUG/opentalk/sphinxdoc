Risorse
=======

Per finire, ecco alcuni link  che possono essere utili.

|hr|
**Generatori di siti statici**

https://www.staticgen.com/ |br|
https://gohugo.io/

|hr|
**Metalinguaggi html**

https://www.markdownguide.org/ |br|
https://docutils.sourceforge.io/rst.html |br|
https://pandoc.org/

|hr|
**Sphinx**

https://www.sphinx-doc.org/en/master/ |br|
https://www.sphinx-doc.org/en/master/examples.html |br|
https://github.com/sphinx-doc/sphinx

|hr|
**Python**

https://python.org/ |br|
https://pypi.org/ |br|
https://pypi.org/search/?q=sphinx


|hr|
**Io**

.. image:: /_static/corso.jpg

mailto:claudio.driussi@gmail.com |br|
https://www.pnlug.it/


