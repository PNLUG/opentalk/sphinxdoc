
Scrivere la documentazione
==========================

Finalmente è arrivato il momento di iniziare a scrivere.

La documentazione è suddivisa in pagine. Ogni pagina compare nella
*"Table of content"* che di solito è una colonna che compare sulla destra o
sulla sinistra del testo principale. Quando si aprono le pagine, il menù di
navigazione si apre includendo l'albero dei titoli che corrisponde al livello
dei titoli html.

Le pagine sono organizzate in modo gerarchico e da ogni pagina può partire un
sotto albero di documentazione.

Come avrete potuto constatare dalla documentazione ufficiale, nonostante sia
abbastanza facile scrivere i primi testi, il sistema è piuttosto articolato
con molte opzioni e può essere difficile da padroneggiare a fondo.

In questo tutorial perciò esamineremo solo alcune delle moltissime possibilità
che offre, e data la vastità dei testi, non includeremo quì i testi completi,
ma solo gli elementi sintattici esaminati. Comunque renderemo disponibili gli
esempi completi ed anche il testo di questa documentazione.

Qualche cenno di RestructuredText
---------------------------------

Come dicevamo in precedenza, il linguaggio di markup privilegiato di Sphinx
è `RestructuredText <https://docutils.sourceforge.io/rst.html>`_. La sintassi
non è complessa ma nemmeno banale ed offre una grande versatilità.

Un buon punto di partenza per imparare è l'`introduzione
<http://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html>`_
che si trova nella documentazione di Sphinx.

Non vale la pena quindi di ripetere quanto già scritto molto meglio di quanto
potrei fare io. Può invece essere utile focalizzare l'attenzione su alcuni
aspetti che riguardano la metodologia di lavoro.

In ogni caso nel progetto di prova ho messo alcuni esempi commentati di molte
delle funzionalità presentate. Ed anche il sorgente del presente testo può
essere esaminato per ricavare spunti.

Commenti
........

I commenti sono sempre fondamentali in ogni progetto. In rst, i commenti si
inseriscono scrivendo due punti sulla sinistra e tenendo indentato tutto il
testo commentato, ad esempio:

::

    .. questo è un commento
       su due righe


Intestazioni
............

In rst le Intestazioni si indicano sottolineando i titoli. Per ogni livello
di intestazione si usa un carattere di sottolineatura diverso e l'ordine in cui
si presentano indica il livello. Se si desidera si può inserire la
sottolineatura anche sopra. Ad esempio:

::

    =========
    Header H1
    =========

    Header H2
    ---------

    Header H3
    +++++++++

    Header H3
    +++++++++

    Header H2
    ---------

    Header H3
    +++++++++

Le intestazioni sono importanti perché vengono usate da Sphinx per costruire
l'albero dell'indice.

Paragrafi
.........

In rst un paragrafo è un insieme di righe di testo senza righe vuote con la
stessa indentazione. Ad esempio

::

    paragrafo su
    due linee

       paragrafo
       indentato
       su tre linee


Elementi tipografici
....................

Naturalmente si possono inserire elementi tipografici come ad esempio
*corsivo*, **grassetto** e ``codice a dimensione fissa``

::

    .. si scrivono così

    *corsivo*, **grassetto** e ``codice a dimensione fissa``

Il sottolineato non è previsto per evitare confusione con i links.


Liste
.....

Si possono anche inserire liste puntate e numerate con diversi automatismi.
la piu semplice è:

- Mele
- Pere
- Albicocche

::

    - Mele
    - Pere
    - Albicocche

Tabelle
.......

Anche le tabelle hanno alcuni parametri di configurazione, ma ad esempio:

==========  ========
Prodotto    Quantità
==========  ========
Mele        kg. 1.5
Pere        kg. 1
Albicocche  kg. 2
==========  ========

::

    ==========  ========
    Prodotto    Quantità
    ==========  ========
    Mele        kg. 1.5
    Pere        kg. 1
    Albicocche  kg. 2
    ==========  ========

Links
.....

Si possono distinguere due tipi di link, i link interni e quelli esterni.

I **link interni** sono relativi alla documentazione, in modo che spostando i
files html compilati i link continuino a funzionare.

Per identificare le pagine si usa il comando `:doc:` ad esempio
:doc:`/pagine/glossario` oppure si può identificare una posizione precisa
inserendo una label nel punto di destinazione e poi un comando `:ref:` nel
punto di chiamata ad esempio :ref:`md_ref`

::

    .. punto di destinazione, da notare il carattere sottolineato iniziale
       che deve essere tolto nel link di chiamata.

    .. _md_ref:

    Markdown
    ........

    .. chiamata

    :ref:`md_ref` oppure  :ref:`Il linguaggio MarkDown <md_ref>`

    posso chiamare una pagina con. :doc:`/pagine/glossario`

    .. immagine

    .. image:: _static/sphinxheader.png



I **link esterni** invece permettono di inserire la URL delle pagine esterne.
La cosa più semplice è quella di inserire nel testo la url completa senza
modifiche ad esempio https://www.python.org/ ed in tal caso il testo
corrisponde alla URL

Oppure si puo inserire un testo a piacere usando una sintassi specifica ad
esempio `Il linguaggio Python <https://www.python.org/>`_

::

    .. ecco un link esterno esplicito

    https://www.python.org/

    .. oppure con un testo proprio

    `Il linguaggio Python <https://www.python.org/>`_

    .. da notare lo spazio tra il testo ed il link ed il carattere di
       sottolineature finale.


Per gli altri elementi rimandiamo alla documentazione ufficiale


Blocchi di codice
.................

Quando si scrive documentazione tecnica, si inseriscono molto spesso blocchi di
codice che devono essere riportati in modo letterale e possibilmente
evidenziati in modo diverso dal resto del codice. Ecco un esempio:

::

    ::

        # questo è un pezzo di codice python
        def foo():
            return "bar"


La struttura del progetto
-------------------------

Un progetto Sphinx è organizzato a pagine.

Ogni pagina per essere visibile deve essere inclusa in una "Table of contents"
con la direttiva ``.. toctree::``, poi nell'indice verranno inclusi i
sottolivelli determinati dai livelli di intestazione.

Per maggiore convenienza, le pagine possono essere contenute in sottocartelle
e nel ``.. toctree::`` si dovrà indicare il percorso completo ma relativo alla
pagina chiamante.

Ogni pagina può avere un proprio ``.. toctree::`` ed in questo modo si possono
strutturare gerarchie di documentazione.

Il sistema è in grado di generare automaticamente alcune pagine che sono:

- ``genindex`` Indice analitico
- ``modindex`` Indice dei moduli python
- ``search`` Pagina per le ricerche

Per i dettagli consultare la documentazione


Roles, Directives, Domains
--------------------------

Elencare in modo esaustivo tutte le funzionalità di Sphinx non è lo scopo di
questa documentazione. Il nostro scopo è quello di fornire gli strumenti per
cercare quello che può servire.

Per questo motivo accenniamo solamente alcune delle caratteristiche che si
possono attivare utilizzando rst. Alcune le abbiamo già viste.

Le **Roles** (Regole) sono parole racchiuse tra due segni dei due punti, ne
abbiamo viste alcune ad esempio ``:ref:`` e  ``:doc:`` parlando dei link
interni.

Altra regola interessante è ``:abbr:`` che permette di gestire abbreviazioni ed
acronimi, ad esempio ``:abbr:`OS (Operating System)``` visualizza la parola
`OS` e fa un popup con `(Operating System)`

Molto utili sono anche le ``substitutions`` che permettono di scrivere campi
variabili ad esempio posso dichiarare:

::

    .. |my_url| replace:: https://mywebagency.com

e poi richiamarle nel testo con ``|my_url|`` se in seguito decido di cambiare
la url è sufficiente cambiare solo la definizione.

Le **Directives** (Direttive) sono comandi che iniziano con due punti ``..`` ad inizio
riga alcune le abbiamo già viste, ad esempio ``.. toctree::`` altre si
trovano nella documentazione, ad esempio ``.. note::``, ``.. versionadd::``
``.. deprecated::`` ed altre.

I **Domains** (Domini) come recita il manuale sono collezioni di directives e
roles che appartengono ad uno stesso gruppo ad esempio gli elementi di un
linguaggio di programmazione.

Sphinx integra domains per alcuni linguaggi di programmazione ed ambienti tra
cui Python, C, JavaScript RestructuredText, Math ed altri. Altri ancora
sono stati resi disponibili come estensioni di terze parti.

